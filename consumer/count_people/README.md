# people-counter

Análise do fluxo de pessoas, gerando eventos no momento que cada pessoa é contada. 

## Instalação Rápida
[Download](https://drive.google.com/file/d/1cewMfusmPjYWbrnuJRuKhPMwRe_b9PaT/view) o arquivo de pesos Darknet YOLO v4 e converta para o modelo Keras:

`python convert.py`

Após isso para executar o demo:
s
`python demo.py`
import cv2
import numpy as np
import sys
import time
from kombu import Connection, Exchange, Queue
from kombu.mixins import ConsumerMixin
from datetime import datetime as dt
import uuid
import threading

from count_people.demo import people_counter

rabbit_url = 'amqp://guest:guest@3.80.172.11:5672//'


def video_writer(frame, frames):
    height, width, layers = frame.shape
    size = (width, height)
    frames.append(frame)
    print(len(frames))
    # each X frames, people_counter is called
    if len(frames) % 120 == 0:
        Model(frames)

def Model(frames):
    result = people_counter(frames)
    print(result)






class Worker(ConsumerMixin):
    def __init__(self, connection, queues):
        self.connection = connection
        self.queues = queues
        self.frames = []



    def get_consumers(self, Consumer, channel):
        return [Consumer(queues=self.queues,
                         callbacks=[self.on_message],
                         accept=['image/jpeg'])]

    def on_message(self, body, message):
        size = sys.getsizeof(body)
        np_array = np.frombuffer(body, dtype=np.uint8)
        image = cv2.imdecode(np_array, 1)
        count = video_writer(image, self.frames)
        print(dt.now())
        message.ack()



def run():
    frames = []
    print("[*] CONSUMER APP rabbitMQ ")
    exchange = Exchange("video-exchange", type="direct")
    print("[*] QUEUE: video-queue")
    queues = [Queue("video-queue", exchange, routing_key="video")]
    print("[*] WORKER RUNNING  ")
    with Connection(rabbit_url, heartbeat=40) as conn:
            worker = Worker(conn, queues)
            print("[*] WORKER RUN RABBIT LOADING...")
            worker.run()

if __name__ == "__main__":
    run()

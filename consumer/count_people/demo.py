#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

from timeit import time
import warnings
import cv2
import numpy as np
from PIL import Image
from count_people.yolo import YOLO
import datetime

from count_people.deep_sort import preprocessing
from count_people.deep_sort import nn_matching
from count_people.deep_sort.detection import Detection
from count_people.deep_sort.detection_yolo import Detection_YOLO
from count_people.deep_sort.tracker import Tracker
from count_people.tools import generate_detections as gdet
import imutils.video


warnings.filterwarnings('ignore')

people_tracking = []


def testIntersectionIn(x, y):
    res = -90 * x + 450 * y - 148500
    if y >= 400:
        return True
    return False


def testIntersectionOut(x, y):
    res = abs(-90 * x + 450 * y - 171000) / np.sqrt((90 * 90) + (450 * 450))
    if res <= 2 and res >= 0:
        return True
    return False

def People_Counter(frame, textIn, textOut,id):
    dt = str(datetime.datetime.now())
    total = textIn + textOut + 1
    result = {'date_time': dt ,
              'total_people': total,
              'id': id,
              'processed_frame':frame }
    print("Time:  ",result['date_time'])
    print("Total People: ", result['total_people'])
    print("Person ID: ", result['id'])
    return result



def people_counter(frames,yolo=YOLO()):
    width = 800
    height = 650
    textIn = 0
    textOut = 0


    # Definition of the parameters
    max_cosine_distance = 0.3
    nn_budget = None
    nms_max_overlap = 1.0

    # Deep SORT
    model_filename = 'count_people/model_data/mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename, batch_size=1)

    metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
    tracker = Tracker(metric)

    # Parameteres
    tracking = True

    # Write video
    out = cv2.VideoWriter('out.avi', cv2.VideoWriter_fourcc(*'XVID'), 15, (width, height))

    for i in range (len(frames)):
        frame = cv2.resize(frames[i], (width, height))
        image = Image.fromarray(frame[..., ::-1])  # bgr to rgb
        boxes, confidence, classes = yolo.detect_image(image)

        if tracking:
            features = encoder(frame, boxes)

            detections = [Detection(bbox, confidence, cls, feature) for bbox, confidence, cls, feature in
                          zip(boxes, confidence, classes, features)]
        else:
            detections = [Detection_YOLO(bbox, confidence, cls) for bbox, confidence, cls in
                          zip(boxes, confidence, classes)]

        # Run non-maxima suppression.
        boxes = np.array([d.tlwh for d in detections])
        scores = np.array([d.confidence for d in detections])
        indices = preprocessing.non_max_suppression(boxes, nms_max_overlap, scores)
        detections = [detections[i] for i in indices]

        for det in detections:
            bbox = det.to_tlbr()
            score = "%.2f" % round(det.confidence * 100, 2) + "%"
            cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (255, 0, 0), 2)
            if len(classes) > 0:
                cls = det.cls
                cv2.putText(frame, str(cls) + " " + score, (int(bbox[0]), int(bbox[3])), 0,
                            1e-3 * frame.shape[0], (0, 255, 0), 1)

        if tracking:
            # Call the tracker
            tracker.predict()
            tracker.update(detections)

            for track in tracker.tracks:  # por pessoa
                if not track.is_confirmed() or track.time_since_update > 1:
                    continue
                bbox = track.to_tlbr()

                w, h = int(bbox[2]) - int(bbox[0]), int(bbox[3]) - int(bbox[1])
                x, y = int(int(bbox[0]) + w / 2), int(int(bbox[1]) + h / 2)

                cv2.line(frame, (width // 4 - 100, width // 2), (width // 2 + 200, 500), (250, 0, 1), 2)  # blue line
                cv2.line(frame, (width // 4 - 50, width // 2 - 50), (width // 2 + 250 ,450 ), (0, 0, 255), 2)  # red line


                rectagleCenterPont = (x, y)
                cv2.circle(frame, rectagleCenterPont, 1, (0, 0, 255), 5)

                if (track.track_id) not in people_tracking and (testIntersectionIn(x, y)):
                    people_tracking.append(track.track_id)
                    textIn += 1
                    result = People_Counter(frame, textIn, textOut, track.track_id)

                if (track.track_id) not in people_tracking and (testIntersectionOut(x, y)):
                    people_tracking.append(track.track_id)
                    result = People_Counter(frame, textIn, textOut, track.track_id)
                    textOut += 1

                cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (255, 255, 255), 2)
                cv2.putText(frame, "ID: " + str(track.track_id), (int(bbox[0]), int(bbox[1])), 0,
                            1e-3 * frame.shape[0], (0, 255, 0), 1)

        cv2.putText(frame, "Pessoas Saindo: " + str(textOut), (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (0, 255, 0), 2)
        cv2.putText(frame, "Pessoas Entrando: " + str(textIn), (100, 150), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (0, 255, 0), 2)

        total = textIn + textOut
        # save a frame
        out.write(frame)
        cv2.imshow('People Counter', frame)



        # Press Q to stop!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    out.release()
    cv2.destroyAllWindows()

    return total
